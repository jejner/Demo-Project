package com.vmware.codestreamdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodestreamDemoApplication {
    //Test....
	public static void main(String[] args) {
		SpringApplication.run(CodestreamDemoApplication.class, args);
	}
}
